import 'package:flutter/material.dart';
import 'products.dart';
import 'profile.dart';

class Home extends StatelessWidget {
  final String _username;
  final String _password;

  const Home({super.key, required String username, required String password})
      : _username = username,
        _password = password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Wellcome to our shop!',
          style: TextStyle(color: Colors.grey, fontStyle: FontStyle.italic),
        ),
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.grey),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          
             const Spacer(flex: 10),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Profile(
                      username: _username,
                      password: _password,
                    ),
                  ),
                );
              },
              child: Text('View Profile'),
            ),
             const Spacer(flex: 3),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Products(),
                  ),
                );
              },
              child: Text('View Products'),
            ),
             const Spacer(flex: 10),
          ],
        ),
      ),
    );
  }
}