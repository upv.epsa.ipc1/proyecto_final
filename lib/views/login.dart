import 'package:flutter/material.dart';
import 'home.dart';
import 'singup.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  String? usernameRegister;
  String? passwordRegister;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Login',
          style: TextStyle(color: Colors.white, fontStyle: FontStyle.italic),
        ),
        backgroundColor: Colors.black,
      ),
      body: Container(
        color: Colors.white, 
        padding: EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center, 
            children: [
              
              const Spacer(flex: 4),
              Container(
                width: 400.0, 
                height: 50.0,
                decoration: BoxDecoration(
                  color: Colors.grey, 
                  borderRadius: BorderRadius.circular(8.0), 
                ),
                child: Row(
                  children: [
                    
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Icon(Icons.person_3_rounded),
                    ),
                    Expanded(
                      child: TextField(
                        controller: _usernameController,
                        decoration: const InputDecoration(
                          hintText: 'Username',
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ],
                ),
              ),

               const Spacer(flex: 1),

            
              Container(
                width: 400.0,
                height: 50.0,
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(8.0), 
                ),
                child: Row(
                  children: [

                    
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Icon(Icons.key),
                    ),
                    Expanded(
                      child: TextField(
                        controller: _passwordController,
                        decoration: InputDecoration(
                          hintText: 'Password',
                          border: InputBorder.none,
                        ),
                        obscureText: true,
                      ),
                    ),
                  ],
                ),
              ),

              
               const Spacer(flex: 2),

            
              ElevatedButton(
                onPressed: () {
                  String username = _usernameController.text;
                  String password = _passwordController.text;

                  if (username == usernameRegister && password == passwordRegister) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Home(
                          username: username,
                          password: password,
                        ),
                      ),
                    );
                  } else {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text('Error'),
                          content: Text('Wrong Username or Password'),
                          actions: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('OK'),
                            ),
                          ],
                        );
                      },
                    );
                  }
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(vertical: 16),
                  child: Text(
                    'LOGIN',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),

          
               const Spacer(flex: 2),

             
              MouseRegion(
                cursor: SystemMouseCursors.click,
                child: GestureDetector(
                  onTap: () async {
                    final result = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Sing_Up()),
                    );

                    if (result != null && result is Map) {
                      setState(() {
                        usernameRegister = result['username'];
                        passwordRegister = result['password'];
                      });
                    }
                  },
                  child: Text(
                    "Sing Up",
                    style: TextStyle(
                      color: Colors.blue,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                ),
              ),

               const Spacer(flex: 3),
            ],
          ),
        ),
      ),
    );
  }
}
