import 'package:flutter/material.dart';
import 'products.dart'; 

class ProductView extends StatelessWidget {
  final Product product;

  const ProductView({required this.product, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Product',
          style: TextStyle(color: Colors.white,fontStyle: FontStyle.italic),
        ),
        backgroundColor:  Colors.black,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
             const Spacer(flex: 1),
            Center(
              child: Image.asset(
                product.image,
                width: 200,
                height: 200,
              ),
            ),
             const Spacer(flex: 2),
            Text(
              product.name,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
             const Spacer(flex: 1),
            Text(
              "\$${product.price}",
              style: TextStyle(fontSize: 18, color: Colors.green),
            ),
             const Spacer(flex: 2),
            Text(
              "Description",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
             const Spacer(flex: 1),
            Text(
              product.description,
              style: TextStyle(fontSize: 16),
            ),
             const Spacer(flex: 2),
          ],
        ),
      ),
    );
  }
}