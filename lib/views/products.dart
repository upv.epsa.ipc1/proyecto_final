import 'package:flutter/material.dart';
import 'dart:convert';
import 'product.dart';

class Product {
  final String name;
  final String image;
  final double price;
  final String description;

  Product({
    required this.name,
    required this.image,
    required this.price,
    required this.description,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      name: json['name'],
      image: json['image'],
      price: double.parse(json['price']),
      description: json['description'],
    );
  }
}

class ProductsList {
  final List<Product> products;

  ProductsList({required this.products});

  factory ProductsList.fromJson(Map<String, dynamic> json) {
    var list = json['products'] as List;
    List<Product> productsList = list.map((i) => Product.fromJson(i)).toList();
    return ProductsList(products: productsList);
  }
}

class Products extends StatelessWidget {
  const Products({Key? key}) : super(key: key);

  Future<String> cargarArchivo(BuildContext context) async {
    var result = await DefaultAssetBundle.of(context).loadString('assets/json/products.json');
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Our Products',
          style: TextStyle(color: Colors.white,fontStyle: FontStyle.italic),
        ),
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: FutureBuilder(
        future: cargarArchivo(context),
        builder: (context, AsyncSnapshot<String> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error'));
          } else if (snapshot.hasData) {
            Map<String, dynamic> data = json.decode(snapshot.data!);
            var products = ProductsList.fromJson(data);
            return ListView.builder(
              itemCount: products.products.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(products.products[index].name),
                  subtitle: Text("${products.products[index].price}"),
                  leading: Image.asset(
                    products.products[index].image,
                    width: 40,
                    height: 40,
                  ),
                  trailing: Icon(Icons.arrow_forward),
                  
                  onTap: () {
                    Navigator.push(
                       context,
                      MaterialPageRoute(
                      builder: (context) => ProductView(product: products.products[index]),
                      ),
                    );
                  }, 
                );
              },
            );
          }
          return Center(child: Text('No products found'));
        },
      ),
    );
  }
}
