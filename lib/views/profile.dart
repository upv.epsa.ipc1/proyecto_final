import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:convert';


class ProfileImage {  
  final String name;
  final String imageUrl;

  ProfileImage({required this.name, required this.imageUrl}); 
  factory ProfileImage.fromJson(Map<String, dynamic> json) {
    return ProfileImage(
      name: json['name'],
      imageUrl: json['image'],
    );
  }
}



class Profile extends StatefulWidget {
  final String username;
  final String password;

  const Profile({required this.username, required this.password, Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}




class _ProfileState extends State<Profile> {
  bool tapadoContra = true;
  File? imagen;
  List<ProfileImage> profileImages = [];
  String? imagenSeleccionado;

 

  Future<void> cargarArchivo(BuildContext context) async { 
    var result = await DefaultAssetBundle.of(context).loadString('assets/json/profile.json');
    Map<String, dynamic> data = json.decode(result);
    List<dynamic> imagesJson = data['profile'];
    setState(() {
      profileImages = imagesJson.map((json) => ProfileImage.fromJson(json)).toList();
    });
  }

  @override
  void initState() {
    super.initState();
    cargarArchivo(context);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Profile',
          style: TextStyle(color: Colors.white,fontStyle: FontStyle.italic),
        ),
        backgroundColor: Colors.black,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Center(
        child: profileImages.isEmpty
            ? CircularProgressIndicator()
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  imagen == null
                      ? CircleAvatar(
                          radius: 50,
                          backgroundImage: imagenSeleccionado == null
                              ? AssetImage('assets/img/Black Square.jpg')
                              : AssetImage(imagenSeleccionado!),
                        )
                      : CircleAvatar(
                          radius: 50,
                          backgroundImage: FileImage(imagen!),
                        ),
                  
                   const Spacer(flex: 2),

                  Text('Username: ${widget.username}'),

                   const Spacer(flex: 1),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Password: '),
                      Text(tapadoContra ? '******' : widget.password),
                      IconButton(
                        icon: Icon(tapadoContra ? Icons.visibility : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            tapadoContra = !tapadoContra;
                          });
                        },
                      ),
                    ],
                  ),

                  
                   const Spacer(flex: 2),

                  // Cambiar imagen
                  ElevatedButton(
                    onPressed: eligeImagen,
                    child: Text('Change your Profile Picture'),
                  ),

                   const Spacer(flex: 2),
                  

                ],
              ),
      ),
    );
  }

  
    Future<void> eligeImagen() async {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Select Profile Picture'),
          content: Container(
            width: 900,
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4, 
                crossAxisSpacing: 10, 
                mainAxisSpacing: 10, 
              ),
              itemCount: profileImages.length,
              itemBuilder: (context, index) {
                return MouseRegion(
                  cursor: SystemMouseCursors.click,
                  child : GestureDetector(
                  onTap: () {
                    setState(() {
                      imagenSeleccionado = profileImages[index].imageUrl;
                      imagen = null; 
                    });
                    Navigator.of(context).pop();
                  },
                  child: Column(
                    children: [
                       const Spacer(flex: 2),
                      CircleAvatar(
                        radius: 50,
                        backgroundImage: AssetImage(profileImages[index].imageUrl),
                      ),
                       const Spacer(flex: 4),
                    ],
                  ),
                ),
                );
              },
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
